<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Hello Bulma!</title>
		<link rel="stylesheet" href="{{ url('css/bulma.min.css') }}">
		<link rel="stylesheet" href="{{ url('css/fontawesome-all.min.css') }}">
	</head>
	<body>
		<section class="section">
			<div class="container is-fullhd">
				@yield('content')
			</div>
		</section>

		<footer class="footer">
			<div class="container">
				<div class="content has-text-centered">
					<p>
						<strong>Bulma</strong> by <a href="https://jgthms.com">Jeremy Thomas</a>. The source code is licensed
						<a href="http://opensource.org/licenses/mit-license.php">MIT</a>. The website content
						is licensed <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY NC SA 4.0</a>.
					</p>
				</div>
			</div>
		</footer>
	</body>
</html>